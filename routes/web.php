<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Packages\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('packages', 'PackagesController@index')->name('packages.index');
    });
