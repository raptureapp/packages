<?php

namespace Rapture\Packages\Commands;

use Illuminate\Console\Command;
use Rapture\Hooks\Facades\Hook;

class EnableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package:enable {package : Package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable rapture package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        updatePackageConfig($this->argument('package'), 'installed');

        Hook::dispatch('package.enabled', $this->argument('package'));

        $this->call('config:clear');
    }
}
