<?php

namespace Rapture\Packages\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Rapture\Hooks\Facades\Hook;
use Rapture\Packages\Providers\PackageProvider;
use Symfony\Component\Finder\SplFileInfo;

class UninstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package:uninstall {package : Package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable rapture package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argument = $this->argument('package');
        $package = PackageProvider::getPackage($argument);

        if (is_null($package)) {
            return;
        }

        Hook::dispatch('package.uninstalling', $argument);

        if ($package->hasMigrations) {
            $migrationPath = 'vendor/' . Str::after($package->path . '/../database/migrations', '/vendor/');

            $this->moveMigrations(Str::of($package->path)->before('/src') . '/database/migrations');

            $this->call('migrate:rollback', [
                '--force' => true,
                '--path' => $migrationPath,
            ]);
        }

        if ($package->hasAssets) {
            Storage::deleteDirectory(public_path($package->name));
        }

        if ($package->hasUninstaller) {
            $uninstallPath = $package->namespace . '\\Setup\\Uninstall';
            $uninstaller = new $uninstallPath($argument);
            $uninstaller->handle();
        }

        $packages = config('packages');

        unset($packages[$argument]);

        file_put_contents(config_path('packages.php'), '<?php' . PHP_EOL . PHP_EOL . 'return ' . varExport($packages) . ';' . PHP_EOL);

        Hook::dispatch('package.uninstalled', $argument);

        $this->call('config:clear');
    }

    private function moveMigrations($directory)
    {
        $filesystem = new Filesystem();

        if (!$filesystem->isDirectory($directory)) {
            return;
        }

        $migrations = collect($filesystem->allFiles($directory))
            ->map(function (SplFileInfo $file) {
                return $file->getFilenameWithoutExtension();
            })
            ->toArray();

        $lastGroup = DB::table('migrations')->orderByDesc('batch')->first();

        DB::table('migrations')->whereIn('migration', $migrations)->update(['batch' => $lastGroup->batch + 1]);
    }
}
