<?php

namespace Rapture\Packages\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\PackageManifest;
use Illuminate\Support\Str;
use Rapture\Hooks\Facades\Hook;
use Rapture\Packages\Providers\PackageProvider;

class InstallCommand extends Command
{
    protected $signature = 'package:install {package : Package} {--setup : Allow the package to modify your file system}';
    protected $description = 'Install a rapture package';

    public function handle(PackageManifest $manifest)
    {
        $argument = $this->argument('package');
        $package = PackageProvider::getPackage($argument);

        if (is_null($package)) {
            $this->error('No such package');
            return;
        }

        Hook::dispatch('package.installing', $argument);

        if ($package->hasMigrations) {
            // Account for Windows
            if (str_contains($package->path, ':')) {
                $migrationPath = 'vendor/' . Str::after($package->path, '\vendor\\');
                $migrationPath = Str::replace('\\', '/', $migrationPath);
                $migrationPath = Str::replace('/src', '/database/migrations', $migrationPath);
            } else {
                $migrationPath = 'vendor/' . Str::after($package->path . '/../database/migrations', '/vendor/');
            }

            $this->call('migrate', [
                '--force' => true,
                '--path' => $migrationPath,
            ]);
        }

        if ($package->hasAssets) {
            $providers = collect($manifest->manifest)->filter(function ($entry, $key) use ($argument) {
                return $key === $argument;
            })->first();

            $this->call('vendor:publish', [
                '--tag' => 'rapture',
                '--force' => true,
                '--provider' => $providers['providers'][0],
            ]);
        }

        if ($package->hasInstaller) {
            $installPath = $package->namespace . '\\Setup\\Install';
            $installer = new $installPath($argument);
            $installer->handle();

            if ($this->option('setup')) {
                $installer->prepareFiles();
            }
        }

        updatePackageConfig($argument, 'installed');

        Hook::dispatch('package.installed', $argument);

        $this->call('config:clear');
    }
}
