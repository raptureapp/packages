<?php

namespace Rapture\Packages\Commands;

use Illuminate\Console\Command;
use Rapture\Hooks\Facades\Hook;

class DisableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package:disable {package : Package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable rapture package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        updatePackageConfig($this->argument('package'), 'disabled');

        Hook::dispatch('package.disabled', $this->argument('package'));

        $this->call('config:clear');
    }
}
