<?php

namespace Rapture\Packages\Facades;

use Illuminate\Support\Facades\Facade;

class PackageDirectory extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'package-directory';
    }
}
