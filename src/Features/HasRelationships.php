<?php

namespace Rapture\Packages\Features;

use Illuminate\Database\Eloquent\Builder;

trait HasRelationships
{
    public function belongsToMany($relationship, $localModel, $foreignModel)
    {
        Builder::macro($relationship, function () use ($relationship, $localModel, $foreignModel) {
            $model = $this->getModel();

            if ($model instanceof $foreignModel) {
                return $model->belongsToMany($localModel, null, null, null, null, null, $relationship);
            }

            unset(static::$macros[$relationship]);

            return $model->{$relationship}();
        });
    }

    public function hasMany($relationship, $localModel, $foreignModel)
    {
        Builder::macro($relationship, function () use ($relationship, $localModel, $foreignModel) {
            $model = $this->getModel();

            if ($model instanceof $foreignModel) {
                return $model->hasMany($localModel);
            }

            unset(static::$macros[$relationship]);

            return $model->{$relationship}();
        });
    }

    public function belongsTo($relationship, $localModel, $foreignModel)
    {
        Builder::macro($relationship, function () use ($relationship, $localModel, $foreignModel) {
            $model = $this->getModel();

            if ($model instanceof $foreignModel) {
                return $model->belongsTo($localModel, null, $model->getKeyName(), $relationship);
            }

            unset(static::$macros[$relationship]);

            return $model->{$relationship}();
        });
    }
}
