<?php

namespace Rapture\Packages;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Rapture\Packages\Commands\DisableCommand;
use Rapture\Packages\Commands\EnableCommand;
use Rapture\Packages\Commands\InstallCommand;
use Rapture\Packages\Commands\UninstallCommand;
use Rapture\Packages\Livewire\PackageList;
use Rapture\Packages\PackageDirectory;

class PackagesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'packages');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'packages');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/packages.php' => config_path('packages.php'),
            ], 'config');
        }

        $this->commands([
            InstallCommand::class,
            DisableCommand::class,
            EnableCommand::class,
            UninstallCommand::class,
        ]);

        Livewire::component('package-list', PackageList::class);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/packages.php', 'packages');

        $this->app->singleton('package-directory', function () {
            return new PackageDirectory();
        });
    }

    public function provides()
    {
        return ['package-directory'];
    }
}
