<?php

namespace Rapture\Packages;

use Rapture\Core\Installer;

class BaseInstall
{
    use Installer;

    public $name;

    public function __construct($package)
    {
        $this->name = $package;
    }

    public function prepareFiles()
    {
    }
}
