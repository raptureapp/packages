<?php

namespace Rapture\Packages\Providers;

use Illuminate\Foundation\PackageManifest;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Rapture\Packages\Features\HasRelationships;
use Rapture\Packages\Package;
use ReflectionClass;

class PackageProvider extends ServiceProvider
{
    use HasRelationships;

    public static $packages = [];

    protected $name = '';
    protected $package;

    public function register()
    {
        $reflector = new ReflectionClass(get_class($this));

        $this->package = new Package();
        $this->package->path(dirname($reflector->getFileName()));
        $this->package->namespace($reflector->getNamespaceName());

        $this->configure($this->package);

        if ($this->package->hasConfig) {
            $this->mergeConfigFrom($this->package->path . '/../config/' . $this->package->configName . '.php', $this->package->configName);
        }

        $this->init();
    }

    public function boot(PackageManifest $manifest)
    {
        $this->name = collect($manifest->manifest)->filter(function ($package) {
            return array_key_exists('providers', $package) && in_array(get_called_class(), $package['providers']);
        })->keys()->first();

        self::$packages[$this->name] = $this->package;

        if (!$this->isEnabled()) {
            return;
        }

        if ($this->package->hasTranslations) {
            $this->loadTranslationsFrom($this->package->path . '/../resources/lang', $this->package->name);
        }

        if ($this->package->hasMigrations) {
            $this->loadMigrationsFrom($this->package->path . '/../database/migrations');
        }

        if ($this->package->hasViews) {
            $this->loadViewsFrom($this->package->path . '/../resources/views', $this->package->name);
        }

        foreach ($this->package->routes as $route) {
            $this->loadRoutesFrom($this->package->path . '/../routes/' . $route . '.php');
        }

        if ($this->app->runningInConsole()) {
            if (!empty($this->package->commands)) {
                $this->commands($this->package->commands);
            }
        }

        if ($this->package->hasAssets) {
            $this->publishes([
                $this->package->path . '/../public' => public_path($this->package->assets),
            ], 'rapture');
        }

        if ($this->package->hasConfig) {
            $this->publishes([
                $this->package->path . '/../config/' . $this->package->configName . '.php' => config_path($this->package->configName . '.php'),
            ], 'config');
        }

        if (!empty($this->package->components)) {
            foreach ($this->package->components as $key => $component) {
                Livewire::component($key, $component);
            }
        }

        if (!empty($this->package->relationships)) {
            foreach ($this->package->relationships as $relationship) {
                $this->{$relationship['type']}($relationship['relationship'], $relationship['model'], $relationship['foreignModel']);
            }
        }

        $this->installed();
    }

    public function configure(Package $package)
    {
    }

    public function installed()
    {
    }

    public function init()
    {
    }

    public function isEnabled()
    {
        $packages = collect(config('packages'));

        return $packages->has($this->name) && $packages->get($this->name) !== 'disabled';
    }

    public static function getPackage($package)
    {
        if (!array_key_exists($package, self::$packages)) {
            return null;
        }

        return self::$packages[$package];
    }
}
