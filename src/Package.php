<?php

namespace Rapture\Packages;

use Illuminate\Support\Str;

class Package
{
    public $name;
    public $fullName;
    public $description;
    public $namespace;
    public $path;
    public $hasConfig = false;
    public $configName = '';
    public $hasViews = false;
    public $hasMigrations = false;
    public $hasTranslations = false;
    public $hasAssets = false;
    public $hasInstaller = false;
    public $hasUninstaller = false;
    public $commands = [];
    public $routes = [];
    public $components = [];
    public $widgets = [];
    public $defaultWidgets = [];
    public $relationships = [];
    public $assets;

    public function name($name, $fullName = '')
    {
        $this->name = $name;
        $this->fullName = $fullName;

        if (empty($this->fullName)) {
            $this->fullName = Str::title($this->name);
        }

        return $this;
    }

    public function description($description)
    {
        $this->description = $description;

        return $this;
    }

    public function path($path)
    {
        $this->path = $path;

        return $this;
    }

    public function namespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    public function config($name = '')
    {
        $this->hasConfig = true;
        $this->configName = $name;

        if (empty($this->configName)) {
            $this->configName = $this->name;
        }

        return $this;
    }

    public function views()
    {
        $this->hasViews = true;

        return $this;
    }

    public function commands($commands)
    {
        $this->commands = array_merge($this->commands, $commands);

        return $this;
    }

    public function widget($widget, $visible = false)
    {
        $key = str($widget)->afterLast('\\')->kebab()->__toString();

        $this->components['widget-' . $key] = $widget;
        $this->widgets[$key] = $widget;

        if ($visible) {
            $this->defaultWidgets[] = $key;
        }

        return $this;
    }

    public function components($components = [])
    {
        foreach ($components as $key => $component) {
            $this->component($key, $component);
        }

        return $this;
    }

    public function component($key, $component)
    {
        $this->components[$key] = $component;

        return $this;
    }

    public function attachRelationship($type, $relationship, $model, $foreignModel)
    {
        $this->relationships[] = [
            'type' => $type,
            'relationship' => $relationship,
            'model' => $model,
            'foreignModel' => $foreignModel,
        ];
    }

    public function attachBelongsToMany($relationship, $model, $foreignModel)
    {
        $this->attachRelationship('belongsToMany', $relationship, $model, $foreignModel);

        return $this;
    }

    public function attachHasMany($relationship, $model, $foreignModel)
    {
        $this->attachRelationship('hasMany', $relationship, $model, $foreignModel);

        return $this;
    }

    public function attachBelongsTo($relationship, $model, $foreignModel)
    {
        $this->attachRelationship('belongsTo', $relationship, $model, $foreignModel);

        return $this;
    }

    public function routes(...$routes)
    {
        $this->routes = $routes;

        return $this;
    }

    public function migrations()
    {
        $this->hasMigrations = true;

        return $this;
    }

    public function translations()
    {
        $this->hasTranslations = true;

        return $this;
    }

    public function installer()
    {
        $this->hasInstaller = true;

        return $this;
    }

    public function uninstaller()
    {
        $this->hasUninstaller = true;

        return $this;
    }

    public function assets($assetKey = '')
    {
        $this->hasAssets = true;
        $this->assets = $assetKey;

        if (empty($this->assets)) {
            $this->assets = $this->name;
        }

        return $this;
    }

    public function properName()
    {
        return $this->fullName;
    }
}
