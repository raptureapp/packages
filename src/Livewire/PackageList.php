<?php

namespace Rapture\Packages\Livewire;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Livewire\Attributes\Url;
use Livewire\Component;
use Rapture\Packages\Providers\PackageProvider;

class PackageList extends Component
{
    #[Url]
    public $filter = 'all';

    #[Url]
    public $search = '';

    #[Url(except: '')]
    public $vendor = '';

    public $searchTerm = '';

    public function command($type, $package)
    {
        $command = 'package:' . $type . ' ' . $package;

        if ($type === 'install') {
            $command .= ' --setup';
        }

        Artisan::call($command);

        $packages = config('packages');

        if ($type === 'uninstall') {
            unset($packages[$package]);
        } elseif ($type === 'disable') {
            $packages[$package] = 'disabled';
        } else {
            $packages[$package] = 'installed';
        }

        config(['packages' => $packages]);

        $this->dispatch('refreshMenu');
    }

    public function applySearch()
    {
        $this->search = $this->searchTerm;
    }

    public function applyFilter($filter)
    {
        $this->filter = $filter;
    }

    public function mount()
    {
        $this->searchTerm = $this->search;
    }

    public function render()
    {
        $packages = collect(PackageProvider::$packages)
            ->reject(function ($package) {
                if (is_null($package)) {
                    return true;
                }

                return $package->name === 'users';
            })
            ->sortBy(function ($package) {
                return $package->name;
            });

        $connected = collect(config('packages'))
            ->reject(function ($status, $package) use ($packages) {
                return $package === 'rapture/users' || !$packages->has($package);
            });

        $total = count($packages);
        $available = count($packages) - count($connected);
        $status = $connected->countBy(function ($status, $package) {
            return $status;
        });

        if ($this->filter === 'enabled') {
            $packages = $packages->filter(function ($package, $name) use ($connected) {
                return $connected->get($name, '') === 'installed';
            });
        }

        if ($this->filter === 'disabled') {
            $packages = $packages->filter(function ($package, $name) use ($connected) {
                return $connected->get($name, '') === 'disabled';
            });
        }

        if ($this->filter === 'available') {
            $packages = $packages->reject(function ($package, $name) use ($connected) {
                return $connected->has($name);
            });
        }

        $vendors = $this->getVendors($packages);

        if (!empty($this->search)) {
            $packages = $packages->filter(function ($package, $name) {
                return strpos($name, $this->search) !== false ||
                    strpos($package->name, $this->search) !== false ||
                    strpos($package->fullName, $this->search) !== false ||
                    strpos($package->description, $this->search) !== false;
            });
        }

        if (!empty($this->vendor)) {
            $packages = $packages->filter(function ($package, $name) {
                return strpos($name, $this->vendor . '/') === 0;
            });
        }

        $packageVersions = $this->getCachedPackageVersions($packages->keys()->toArray());

        $packages = $packages->map(function ($package, $key) use ($packageVersions) {
            $versionInfo = $packageVersions[$key] ?? ['installed' => 'Unknown', 'latest' => 'Unknown'];
            $package->version = $versionInfo['installed'];
            $package->latestVersion = $versionInfo['latest'];
            $package->hasUpdate = !in_array($package->version, ['dev-master', 'dev-main']) &&
                version_compare($versionInfo['latest'], $versionInfo['installed'], '>');
            return $package;
        });

        return view('packages::list', [
            'packages' => $packages,
            'connected' => $connected->toArray(),
            'available' => $available,
            'status' => $status,
            'total' => $total,
            'vendors' => $vendors,
        ]);
    }

    private function getCachedPackageVersions(array $packageNames): array
    {
        $cacheKey = 'package_versions_' . md5(implode('', $packageNames));

        return Cache::remember($cacheKey, now()->addHour(), function () use ($packageNames) {
            return $this->getPackageVersions($packageNames);
        });
    }

    private function getPackageVersions(array $packageNames): array
    {
        $lockFile = base_path('composer.lock');

        if (!file_exists($lockFile)) {
            return [];
        }

        $lockContent = json_decode(file_get_contents($lockFile), true);
        $versions = [];

        foreach ($lockContent['packages'] as $package) {
            if (!in_array($package['name'], $packageNames)) {
                continue;
            }

            $installedVersion = $package['version'];
            if (in_array($installedVersion, ['dev-master', 'dev-main'])) {
                $versions[$package['name']] = [
                    'installed' => $installedVersion,
                    'latest' => $installedVersion,
                ];
            } else {
                $versions[$package['name']] = [
                    'installed' => $installedVersion,
                    'latest' => $this->getLatestVersion($package['name']),
                ];
            }
        }

        return $versions;
    }

    private function getLatestVersion(string $packageName): string
    {
        $endpoints = [
            "https://repo.rapture.dev/p2/{$packageName}.json",
            "https://repo.packagist.org/p2/{$packageName}.json",
        ];

        foreach ($endpoints as $endpoint) {
            $response = Http::get($endpoint);

            if ($response->successful()) {
                $data = $response->json();
                $versions = $data['packages'][$packageName] ?? [];

                if (!empty($versions)) {
                    $latestVersion = array_key_first($versions);
                    return $versions[$latestVersion]['version'];
                }
            }
        }

        return 'Unknown';
    }

    private function getVendors($packages)
    {
        return $packages->map(function ($package, $name) {
            return ucfirst(explode('/', $name)[0]);
        })->unique()->values()->toArray();
    }
}
