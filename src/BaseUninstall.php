<?php

namespace Rapture\Packages;

class BaseUninstall
{
    public $name;

    public function __construct($package)
    {
        $this->name = $package;
    }
}
