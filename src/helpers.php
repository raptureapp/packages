<?php

function varExport($variable)
{
    $export = var_export($variable, true);
    $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
    $array = preg_split("/\r\n|\n|\r/", $export);
    $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
    $export = join(PHP_EOL, array_filter(["["] + $array));

    return $export;
}

function updatePackageConfig($name, $status)
{
    $packages = config('packages');
    $packages[$name] = $status;

    file_put_contents(config_path('packages.php'), '<?php' . PHP_EOL . PHP_EOL . 'return ' . varExport($packages) . ';' . PHP_EOL);
}
