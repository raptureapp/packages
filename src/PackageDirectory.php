<?php

namespace Rapture\Packages;

use Illuminate\Support\Str;
use Rapture\Packages\Providers\PackageProvider;

class PackageDirectory
{
    public $packages;

    public function __construct()
    {
        $installed = collect(config('packages'));

        $this->packages = collect(PackageProvider::$packages)
            ->filter(fn ($package, $key) => !is_null($package) && $installed->has($key) && $installed->get($key) !== 'disabled')
            ->sortBy(fn ($package) => $package->name);
    }

    public function contains($name = '')
    {
        return $this->packages->has($name);
    }

    public function widgets()
    {
        return $this->packages->reject(fn ($package) => empty($package->widgets));
    }

    public function defaultWidgets()
    {
        return $this->packages
            ->reject(fn ($package) => empty($package->defaultWidgets))
            ->reduce(fn ($final, $package) => array_merge($final, $package->defaultWidgets), []);
    }
}
