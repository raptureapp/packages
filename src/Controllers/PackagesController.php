<?php

namespace Rapture\Packages\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function index()
    {
        return view('packages::index');
    }
}
