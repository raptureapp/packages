<div>
    <x-heading>
        <x-h1>@lang('packages::package.plural')</x-h1>
    </x-heading>

    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 mt-2 sm:mt-4" x-data="{ tab: @entangle('filter').live }">
        <x-tabs>
            <x-tab key="all" label="All Packages" :badge="$total" />
            @if ($status->has('installed'))
                <x-tab key="enabled" label="Enabled" :badge="$status->get('installed')" />
            @endif
            @if ($status->has('disabled'))
                <x-tab key="disabled" label="Disabled" :badge="$status->get('disabled')" />
            @endif
            @if ($available > 0)
                <x-tab key="available" label="Available" :badge="$available" />
            @endif

            <x-form class="flex-1 flex justify-end items-center mb-2 space-x-4" wire:submit.prevent="applySearch">
                <x-select wire:model.live="vendor" class="w-48">
                    <option value="">All Vendors</option>
                    @foreach ($vendors as $vendorName)
                    <option value="{{ strtolower($vendorName) }}">{{ $vendorName }}</option>
                    @endforeach
                </x-select>
                <x-input placeholder="Search" type="search" name="search" id="search" wire:model="searchTerm">
                    <x-slot:prepend>
                        <em class="far fa-search"></em>
                    </x-slot:prepend>
                </x-input>
            </x-form>
        </x-tabs>
    </div>

    <x-statuses />

    <x-container>
        <div class="relative overflow-hidden">
            <div wire:loading.flex class="justify-center items-center absolute top-0 left-0 w-full h-full z-10 bg-white dark:bg-slate-900 text-xl opacity-75">
                <em class="far fa-spinner fa-pulse mr-3"></em>
                <p>Loading</p>
            </div>

            <div class="space-y-3">
                @foreach ($packages as $name => $package)
                <x-box wire:key="package-{{ $package->name }}">
                    <div class="flex items-center justify-between p-4 sm:p-6 -m-4 sm:-m-6 rounded-lg border-l-8 {{ !array_key_exists($name, $connected) ? 'border-l-gray-400' : (($connected[$name] === 'disabled') ? 'border-l-red-600 dark:border-l-red-700' : 'border-l-green-600 dark:border-l-green-700') }}">
                        <div>
                            <div class="flex items-center space-x-4">
                                <p class="font-medium">{{ $package->properName() }}</p>
                                <x-badge size="small">{{ $package->version }}</x-badge>
                                @if($package->hasUpdate)
                                    <x-badge size="small" color="yellow">Update available: {{ $package->latestVersion }}</x-badge>
                                @endif
                            </div>
                            <p class="text-sm mt-2">{{ $package->description ?? $name }}</p>
                        </div>

                        <div class="flex items-center space-x-4">
                            @if (!array_key_exists($name, $connected))
                                <x-button wire:click="command('install', '{{ $name }}')" color="primary">Install</x-button>
                            @elseif ($connected[$name] === 'disabled')
                                <x-button wire:click="command('enable', '{{ $name }}')" color="primary">Enable</x-button>
                                <x-button wire:click="command('uninstall', '{{ $name }}')" outline>Uninstall</x-button>
                            @else
                                <x-button wire:click="command('install', '{{ $name }}')" outline>Reinstall</x-button>
                                <x-button wire:click="command('disable', '{{ $name }}')">Disable</x-button>
                                <x-button wire:click="command('uninstall', '{{ $name }}')" outline>Uninstall</x-button>
                            @endif
                        </div>
                    </div>
                </x-box>
                @endforeach

                @if($packages->isEmpty())
                <x-box>
                    <p>No results match your query.</p>
                </x-box>
                @endempty
            </div>
        </div>
    </x-container>
</div>
